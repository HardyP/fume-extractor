// 0=Fume und Aufsteller zusammen, 1=Fume, 2=Aufsteller
// 0=Fume and stand together, 1=Fume, 2=Stand 
DoIt=0; 

// false=Ohne Aufsteller, true=Mit Aufsteller
// false=Without stand, true=With stand
MitAufsteller=true; 

 YI=72.0; // Y Innen, Y inside, 62.0==>60mm, 72.0==>70mm, 82.0==>80mm, 94.0==>92mm
 XI=72.0; // X Innen, X inside, 62.0==>60mm, 72.0==>70mm, 82.0==>80mm, 94.0==>92mm
 DF=67.0; // Durchmesser Luefter, Diameter fan, 57.0==>60mm,  67.0==>70mm, 77.0==>80mm, 89.0==>92mm
 BF=61.5; // Bohrabstand Luefter, Drilling distance fan, 50.0==>60mm, 61.5==>70mm, 71.5==>80mm, 82.5==>92mm

ZA=150.0; // Z Aussen. Z Outside
XAUF=30.0; // X Aufweitung, X Expansion
DS=8.5; // Durchmesser Stecker, Diameter connector

LA=35.0; // Laenge Aufsteller, Length of stand unit
MDA=4.5; // Magnet Durchmesser Aufsteller, Magnet diameter stand
MHA=2.1; // Magnet Hoehe Aufsteller, Magnet Height of stand

// ---------------------------------------------------------------------------
// -------- Ab hier besser nicht mehr aendern ---------------
// -------- Better not change from here on out --------------
// ---------------------------------------------------------------------------

WS=2.8; // WandStaerke, Wall thickness
RDI=5.5; // Rundung Innen, Rounding inside
RDA=8.0; // Rundung Aussen, Outside rounding
YA=YI+WS; // Y Aussen, Y Outside
XA=XI+WS; // X Aussen, X Outside
ZF=5.0; // Z Luefter, Z Fan
DBF=2.0; // Durchmesser Befestigungsloecher des Luefters, Diameter of fan mounting holes
ZS=50.0; // Z Stecker, Z plug
ZBF=30.0;  // Z Befestigung Aufsteller, Z Mounting Stand
DB=3.0; // Durchmesser Bolzen, Bolt diameter

$fn=120;

module Stand(Off=0.0)
{
    difference()  
    {
        union()
        {
             hull()
            {
                translate([-XA/2+RDA/2,-YA/2-DB*1.5,ZBF]) rotate([0,90,0]) cylinder(h=XA-RDA,d=DB*3+Off);
                translate([-XA/2+RDA/2,-YA/2+DB*1.5,ZBF-DB*3]) rotate([0,90,0]) cylinder(h=XA-RDA,d=DB*3+Off);
                translate([-XA/2+RDA/2,-YA/2+DB*1.5,ZBF+DB*3]) rotate([0,90,0]) cylinder(h=XA-RDA,d=DB*3+Off);
            }
        }
        union()
        {
            if (MitAufsteller)
            {
                translate([-XA/2,-YA/2-DB*1.5,ZBF]) rotate([0,90,0]) cylinder(h=XA,d=DB/2);
                translate([-XA/2+RDA/2+10+Off/2,-YA/2-DB*3,ZBF-DB*4]) cube([12-Off,DB*3,DB*8]); 
                translate([XA/2-RDA/2-22+Off/2,-YA/2-DB*3,ZBF-DB*4]) cube([12-Off,DB*3,DB*8]);
            }
            else {translate([-XA/2+RDA/2+10+Off/2,-YA/2-DB*3,ZBF-DB*4]) cube([XA-20-RDA,DB*3,DB*8]);} 
        }
    }
}

module Fume()
{
    difference()
    {
        union()
        {
            hull()
            {
                translate([-XA/2+RDA/2,-YA/2+RDA/2,0]) cylinder(h=ZA,d=RDA);
                translate([XA/2-RDA/2,-YA/2+RDA/2,0]) cylinder(h=ZA,d=RDA);
                translate([-XA/2+RDA/2,YA/2-RDA/2,0]) cylinder(h=ZA,d=RDA);
                translate([XA/2-RDA/2,YA/2-RDA/2,0]) cylinder(h=ZA,d=RDA);
            }
             hull()
            {
                translate([-XA/2+RDA/2,-YA/2+RDA/2,ZA-XAUF]) sphere(RDA/2);
                translate([XA/2-RDA/2,-YA/2+RDA/2,ZA-XAUF]) sphere(RDA/2);
                translate([-XA/2+RDA/2,YA/2-RDA/2,ZA-XAUF]) sphere(RDA/2);
                translate([XA/2-RDA/2,YA/2-RDA/2,ZA-XAUF]) sphere(RDA/2);
                translate([-XA/2+RDA/2-XAUF,-YA/2+RDA/2,ZA]) sphere(RDA/2);
                translate([XA/2-RDA/2+XAUF,-YA/2+RDA/2,ZA]) sphere(RDA/2);
                translate([-XA/2+RDA/2-XAUF,YA/2-RDA/2,ZA]) sphere(RDA/2);
                translate([XA/2-RDA/2+XAUF,YA/2-RDA/2,ZA]) sphere(RDA/2);
            }
            if (MitAufsteller)
            {
                hull()
                {
                    translate([0,-YA/2,ZBF+LA-11]) rotate([90,0,0]) cylinder(h=MHA+0.8,d1=MDA*2.2,d2=MDA*1.5); 
                    translate([0,-YA/2,ZBF+LA-11+MDA/2]) rotate([90,0,0]) cylinder(h=MHA+0.8,d1=MDA*2.2,d2=MDA*1.5);
                } 
                }
            Stand();
       }
        union()
        { 
            hull()
            {
                translate([-XI/2+RDI/2,-YI/2+RDI/2,ZF]) cylinder(h=ZA,d=RDI);
                translate([XI/2-RDI/2,-YI/2+RDI/2,ZF]) cylinder(h=ZA,d=RDI);
                translate([-XI/2+RDI/2,YI/2-RDI/2,ZF]) cylinder(h=ZA,d=RDI);
                translate([XI/2-RDI/2,YI/2-RDI/2,ZF]) cylinder(h=ZA,d=RDI);
            }
             hull()
            {
                translate([-XI/2+RDI/2,-YI/2+RDI/2,ZA-XAUF]) sphere(RDI/2);
                translate([XI/2-RDI/2,-YI/2+RDI/2,ZA-XAUF]) sphere(RDI/2);
                translate([-XI/2+RDI/2,YI/2-RDI/2,ZA-XAUF]) sphere(RDI/2);
                translate([XI/2-RDI/2,YI/2-RDI/2,ZA-XAUF]) sphere(RDI/2);
                translate([-XI/2+RDI/2-XAUF,-YI/2+RDI/2,ZA]) sphere(RDI/2);
                translate([XI/2-RDI/2+XAUF,-YI/2+RDI/2,ZA]) sphere(RDI/2);
                translate([-XI/2+RDI/2-XAUF,YI/2-RDI/2,ZA]) sphere(RDI/2);
                translate([XI/2-RDI/2+XAUF,YI/2-RDI/2,ZA]) sphere(RDI/2);
            }
            translate([-XA*2,-YA*2,ZA]) cube([XA*4,YA*4,10]); 
            translate([-XA*0.75,0,ZS]) rotate([0,90,0]) cylinder(h=XA/2,d=DS);
            translate([-BF/2,-BF/2,-0.1]) cylinder(h=ZF*2,d=DBF);
            translate([BF/2,-BF/2,-0.1]) cylinder(h=ZF*2,d=DBF);
            translate([-BF/2,BF/2,-0.1]) cylinder(h=ZF*2,d=DBF);
            translate([BF/2,BF/2,-0.1]) cylinder(h=ZF*2,d=DBF);
            translate([0,0,-0.1]) cylinder(h=ZF*2,d=DF);
            if (MitAufsteller)
            {
                translate([0,-YA/2,ZBF+LA-10]) rotate([90,0,0]) cylinder(h=MHA,d=MDA); 
                translate([-MDA/2,-YA/2,ZBF+LA-10]) rotate([90,0,0]) cube([MDA,MDA/1.8,MHA]); 
            }
       }
   }
}

module Aufsteller()
{
     difference()
    {
        union()
        {
            translate([-XA/2+RDA/2,-YA/2-DB*3,ZBF]) cube([XA-RDA,DB,LA-5]); 
            hull()
            {
                translate([-XA/2+RDA/2+RDI/2,-YA/2-DB*2,ZBF+LA-RDI/2]) rotate([90,0,0]) cylinder(h=DB,d=RDI); 
                translate([-XA/2+RDA/2+RDI/2,-YA/2-DB*2,ZBF+LA-RDI*2]) rotate([90,0,0]) cylinder(h=DB,d=RDI); 
                translate([-XA/2+RDA/2+RDI*3,-YA/2-DB*2,ZBF+LA-RDI*2]) rotate([90,0,0]) cylinder(h=DB,d=RDI); 
            }
            translate([0,-YA-DB*5,0]) rotate([0,0,180]) hull()
            {
                translate([-XA/2+RDA/2+RDI/2,-YA/2-DB*2,ZBF+LA-RDI/2]) rotate([90,0,0]) cylinder(h=DB,d=RDI); 
                translate([-XA/2+RDA/2+RDI/2,-YA/2-DB*2,ZBF+LA-RDI*2]) rotate([90,0,0]) cylinder(h=DB,d=RDI); 
                translate([-XA/2+RDA/2+RDI*3,-YA/2-DB*2,ZBF+LA-RDI*2]) rotate([90,0,0]) cylinder(h=DB,d=RDI); 
            }
            translate([-XA/2+RDA/2,-YA/2-DB*1.5,ZBF]) rotate([0,90,0]) cylinder(h=XA-RDA,d=DB*3);
            translate([0,-YA/2-DB-MHA/2+0.5,ZBF+LA-10]) rotate([90,0,0]) cylinder(h=MHA+0.5,d1=MDA*1.5,d2=MDA*2.2); 
        }
        union()
        {
            translate([-XA/2,-YA/2-DB*1.5,ZBF]) rotate([0,90,0]) cylinder(h=XA,d=DB/2);
            translate([0,-YA/2-DB-MHA/2,ZBF+LA-10]) rotate([90,0,0]) cylinder(h=MHA*2,d=MDA); 
            Stand(Off=1);
        }
    }
 }

module Zusammen()
{
    color("lightblue",1) Fume();
    if (MitAufsteller) {color("lightgreen",1) Aufsteller();}    
}
 
if (DoIt==0) {Zusammen();} 
else if (DoIt==1) {Fume();} 
else if (MitAufsteller) {rotate([90,0,0]) Aufsteller();}


   
