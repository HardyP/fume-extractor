# fume-extractor
simple solder fume extractor for hobby area

<img src="/20190911-145425.png" alt="TitelBild"/>

Meine Veröffentlichung ist unter https://www.thingiverse.com/thing:3857718 und https://gitlab.com/HardyP/fume-extractor zu finden.

Diese Lötrauchabsaugung ist nicht gedacht um Geräte mit Filter an einem professionellen Arbeitsplatz zu ersetzen. Aber auch im Hobby-Bereich ist es nicht sehr förderlich die Lötdämpfe einzuatmen, die unweigerlich entstehen. Für diesen Anwendungzweck habe ich diese simple Lötrauchabsaugung entwickelt.

Als Basis dient bei mir ein 70er CPU-Lüfter, der seit Jahren tatenlos in einer Elektronik-Kiste lag. Da ich das Gerät in OpenScad entwickelt habe, und nätürlich auch den SCAD-File zur Verfügung stelle, können beliebige Maße eingestellt werden. Für Leute die sich mit OpenScad nicht auskennen, habe passende STL-Dateien für 80er- und 92er-Lüfter beigelegt.

Auf Wunsch kann die Lötrauchabsaugung auch mit einem verstellbaren Aufsteller gedruckt werden, dadurch wird der Neigungswinkel verbessert. Die Achse des Aufstellers bildet ein Stück Filament. Damit die Filament-Achse eingeführt werden kann, sollten die gedrückten Bohrungen mit 2,00mm aufgebohrt werden. Damit der Aufsteller in Ruheposition arritiert ist, kann man während des Druckes kleine Magnete einlegen. Dabei unbedingt auf die magnetische Polung achten. Verwendet werden runde Magnete mit 4mm Durchmesser und 2mm Dicke. Am Aufsteller kommet 1 Magnet rein, am Gerät selber kommen 2 Magnete übereinander. Zum Einlegen der Magnete im Slicer an der passenden Stelle einen Filamentwechsel einstellen, und nach dem Einlegen der Magnete den Druck fortsetzen.

Wie auf einem Foto ersichtlich, habe ich zusätzlich ein Stück 12V-LED-Streifen eingeklebt, diese Option finde ich sehr sinnvoll. Zu Stromversorgung habe ich ein altes Steckernetzteil einer ausgedienten externen Festplatte verwendet, und über einen passenden Stecker verbunden.

Druckparameter: <br />
40% Fülldichte <br />
0,2mm Schichthöhe <br />
4 vertikale Konturen <br />
4 horizontale Konturhüllen <br />
80mm/s Druckgeschwindigkeit

Bei einem gelungen Druck würde ich mich über einen Like sehr freuen! <br />
Gruß Hardy

-----------------------------------------------------------------------------

My publication can be found at https://www.thingiverse.com/thing:3857718 and https://gitlab.com/HardyP/fume-extractor .

This solder fume extraction system is not intended to replace devices with filters at a professional workplace. But also in the hobby area it is not very beneficial to inhale the solder fumes, which inevitably arise. For this purpose I have developed this simple solder fume extractor.

My base is a 70 CPU fan, which has been lying idly in an electronics box for years. Since I have developed the device in OpenScad, and of course also make the SCAD file available, any dimensions can be set. For people who are not familiar with OpenScad, I included suitable STL files for 80 and 92 fans.

If desired, the solder fume extraction can also be printed with an adjustable stand, which improves the angle of inclination. The axis of the stand is a piece of filament. To allow the filament axis to be inserted, the pressed holes should be drilled with 2.00mm. Small magnets can be inserted during printing to ensure that the stand is in the rest position. Pay attention to the magnetic polarity. Round magnets with 4mm diameter and 2mm thickness are used. At the stand 1 magnet comes in, at the device 2 magnets come on top of each other. To insert the magnets in the slicer, set a filament change at the appropriate place and continue the pressure after inserting the magnets.

As you can see on a photo, I also glued in a piece of 12V LED strip, this option makes a lot of sense to me. For power supply I used an old plug-in power supply of a disused external hard disk and connected it with a suitable plug.

Print parameters: <br />
40% filling density <br />
0,2mm layer height <br />
4 vertical contours <br />
4 horizontal contour sleeves <br />
80mm/s Print speed

With a successful print I would be very happy about a Like! <br />
Greetings Hardy
